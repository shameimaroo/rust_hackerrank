fn main() {
    // the string length
    let mut str_n_len = String::new();
    std::io::stdin().read_line(&mut str_n_len).unwrap();
    
    let _: usize = str_n_len.trim().parse().unwrap();
    
    // unencrypted string
    let mut str_line = String::new();
    std::io::stdin().read_line(&mut str_line).unwrap();
    
    // the number of letter shift
    let mut str_shift = String::new();
    std::io::stdin().read_line(&mut str_shift).unwrap();
    
    let n_shift: u8 = str_shift.trim().parse().unwrap();
    
    // shift letters
    for c in str_line.chars() {
        if 'a' <= c && c <= 'z' {
            print!("{}", (('a' as u8) + ((c as u8) - ('a' as u8) + n_shift) % 26) as char);
        } else if 'A' <= c && c <= 'Z' {
            print!("{}", (('A' as u8) + ((c as u8) - ('A' as u8) + n_shift) % 26) as char);
        } else {
            print!("{}", c);
        }
    }
    
    // display the result
    println!("")
}

