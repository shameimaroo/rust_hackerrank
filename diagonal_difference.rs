use std::io;

fn main() {
    // the number of input numbers
    let mut str_n = String::new();
    io::stdin().read_line(&mut str_n).ok().expect("input error: str_n");
    
    let n = str_n.trim().parse().ok().expect("parse error: n");
    
    // the input matrix
    let mut sum_a: i64 = 0;
    let mut sum_b: i64 = 0;
    
    for row in 0..n {
        // one input line
        let mut str_line = String::new();
        io::stdin().read_line(&mut str_line).ok().expect("input error: str_line");
        
        // interpret numbers in the line
        let vec_nums: Vec<&str> = str_line.split(' ').collect();

        // determine the summations
        sum_a += vec_nums[row].trim().parse().ok().expect("parse error: a");
        sum_b += vec_nums[n - row - 1].trim().parse().ok().expect("parse error: b");
    }
    
    // display the output
    println!("{}", (sum_a - sum_b).abs());
}

