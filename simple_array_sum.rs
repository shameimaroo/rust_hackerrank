use std::io;

fn main() {
    // the number of input numbers
    let mut str_n = String::new();
    io::stdin().read_line(&mut str_n).ok().expect("input error: n");
    
    let n: i32 = str_n.trim().parse().ok().expect("parse error: n");
    
    // the input array
    let mut str_arr = String::new();
    io::stdin().read_line(&mut str_arr).ok().expect("input error: array");
    
    let mut s: u64 = 0;
    for str_num in str_arr.split(' ') {
        s += str_num.trim().parse().ok().expect("parse error: array");
    }
    
    // display the result
    println!("{}", s);
}

