fn digit_str(d: u64) -> String {
    match d {
        1 => "one".to_string(),
        2 => "two".to_string(),
        3 => "three".to_string(),
        4 => "four".to_string(),
        5 => "five".to_string(),
        6 => "six".to_string(),
        7 => "seven".to_string(),
        8 => "eight".to_string(),
        9 => "nine".to_string(),
        10 => "ten".to_string(),
        11 => "eleven".to_string(),
        12 => "twelve".to_string(),
        13 => "thirteen".to_string(),
        14 => "fourteen".to_string(),
        15 => "fifteen".to_string(),
        16 => "sixteen".to_string(),
        17 => "seventeen".to_string(),
        18 => "eighteen".to_string(),
        19 => "nineteen".to_string(),
        20 => "twenty".to_string(),
        21...29 => {
            let mut s = "twenty ".to_string();
            s.push_str(&digit_str(d % 20));
            s
        },
        30 => "thirty".to_string(),
        31...39 => {
            let mut s = "thirty ".to_string();
            s.push_str(&digit_str(d % 30));
            s
        },
        40 => "fourty".to_string(),
        41...49 => {
            let mut s = "fourty ".to_string();
            s.push_str(&digit_str(d % 40));
            s
        },
        50 => "fifty".to_string(),
        51...59 => {
            let mut s = "fifty ".to_string();
            s.push_str(&digit_str(d % 50));
            s
        },
        _ => "".to_string(),
    }
}

fn time_str(h: u64, m: u64) -> String {
    match m {
        0 => {
            let mut s = digit_str(h);
            s.push_str(" o' clock");
            s
        },
        1 => {
            let mut s = digit_str(m);
            s.push_str(" minute past ");
            s.push_str(&digit_str(h));
            s
        },
        15 => {
            let mut s = "quarter past ".to_string();
            s.push_str(&digit_str(h));
            s
        },
        m if m < 30 => {
            let mut s = digit_str(m);
            s.push_str(" minutes past ");
            s.push_str(&digit_str(h));
            s
        },
        30 => {
            let mut s = "half past ".to_string();
            s.push_str(&digit_str(h));
            s
        },
        45 => {
            let mut s = "quarter to ".to_string();
            s.push_str(&digit_str(h + 1));
            s
        },
        59 => {
            let mut s = "one minute to ".to_string();
            s.push_str(&digit_str(h + 1));
            s
            
        }
        m if m > 30 => {
            let mut s = digit_str(60 - m);
            s.push_str(" minutes to ");
            s.push_str(&digit_str(h + 1));
            s
        },
        _ => "".to_string(),
    }
}

fn main() {
    // the input hour and minutes
    let mut str_h = String::new();
    std::io::stdin().read_line(&mut str_h).unwrap();
    
    let h: u64 = str_h.trim().parse().unwrap();
    
    let mut str_m = String::new();
    std::io::stdin().read_line(&mut str_m).unwrap();
    
    let m: u64 = str_m.trim().parse().unwrap();
    
    // the cases of hour and minute combination
    println!("{}", time_str(h, m));
}

