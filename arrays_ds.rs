fn main() {
    // the number of inputs
    let mut str_n = String::new();
    std::io::stdin().read_line(&mut str_n).unwrap();
    
    let n: usize = str_n.trim().parse().unwrap();
    
    // the input numbers
    let mut str_line = String::new();
    std::io::stdin().read_line(&mut str_line).unwrap();
    
    // display the result
    let vec_str: Vec<&str> = str_line.trim().split_whitespace().collect();
    for i in 0..n {
        match i {
            0 => print!("{}", vec_str[n - 1 - i]),
            _ => print!(" {}", vec_str[n - 1 - i]),
        }
    }
}

