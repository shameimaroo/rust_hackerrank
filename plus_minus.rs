use std::io;
use std::cmp::Ordering;

fn main() {
    // the number of input numbers
    let mut str_n = String::new();
    io::stdin().read_line(&mut str_n).ok().expect("input error: str_n");
    
    let n: u64 = str_n.trim().parse().ok().expect("parse error: n");
    
    // the input numbers
    let mut str_nums = String::new();
    io::stdin().read_line(&mut str_nums).ok().expect("input error: str_nums");

    // count elements
    let mut neg_cnt: u64 = 0;
    let mut zero_cnt: u64 = 0;
    let mut pos_cnt: u64 = 0;

    let mut num: i64;
    for str_num in str_nums.split(' ') {
        num = str_num.trim().parse().ok().expect("parse error: str_num");
        match 0.cmp(&num) {
            Ordering::Less => pos_cnt += 1,
            Ordering::Equal => zero_cnt += 1,
            Ordering::Greater => neg_cnt += 1,
        }
    }
    
    // display the results
    println!("{:.6}", (pos_cnt as f64) / (n as f64));
    println!("{:.6}", (neg_cnt as f64) / (n as f64));
    println!("{:.6}", (zero_cnt as f64) / (n as f64));
}

