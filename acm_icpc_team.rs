fn main() {
    // the numbers of persons and topics
    let mut str_line = String::new();
    std::io::stdin().read_line(&mut str_line).unwrap();
    
    let vec_line: Vec<&str> = str_line.trim().split_whitespace().collect();
    let n_persons: usize = vec_line[0].parse().unwrap();
    let n_topics: usize = vec_line[1].parse().unwrap();
    
    // the input data
    let mut vec_data: Vec<Vec<char>> = Vec::new();
    for _ in 0..n_persons {
        let mut str_line = String::new();
        std::io::stdin().read_line(&mut str_line).unwrap();
        vec_data.push(str_line.chars().collect());        
    }
    
    let mut vec_persons = vec![0; n_topics];
    
    // simulate binary OR operation on each pair of persons
    let mut n_max_topics: usize = 0;
    for i in 0..n_persons {
        for j in (i + 1)..n_persons {
            let mut n_len: usize = 0;
            for k in 0..n_topics {
                if vec_data[i][k] == '1' || vec_data[j][k] == '1' {
                    n_len += 1;
                }
            }
            
            if n_max_topics < n_len {
                n_max_topics = n_len;
            }
            
            vec_persons[n_len - 1] += 1;
        }
    }
    
    // display the result
    println!("{}", n_max_topics);
    println!("{}", vec_persons[n_max_topics - 1 ] );
}

