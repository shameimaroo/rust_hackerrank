fn main() {
    // the input line
    let mut str_line = String::new();
    std::io::stdin().read_line(&mut str_line).ok().expect("input error: str_line");
    
    // the input numbers
    let arr_time: Vec<&str> = str_line.split(':').collect();
    let mut h: i32 = arr_time[0].trim().parse().ok().expect("parse error: h");
    let m: i32 = arr_time[1].trim().parse().ok().expect("parse error: m");
    let s: i32 = arr_time[2][0..2].trim().parse().ok().expect("parse error: s");
    let str_day = arr_time[2][2..4].to_string();
    
    // AM or PM
    if str_day == "PM" && h != 12 {
        h += 12;
    }
    
    if str_day == "AM" && h == 12 {
        h = 0;
    }
    
    // display the result
    println!("{:02}:{:02}:{:02}", h, m, s);   
}

