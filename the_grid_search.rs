fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases: usize = str_n_cases.trim().parse().unwrap();
    
    // handle each test case
    for _ in 0..n_cases {
        // the target input
        let mut str_size_target = String::new();
        std::io::stdin().read_line(&mut str_size_target).unwrap();
        
        let v1: Vec<&str> = str_size_target.trim().split_whitespace().collect();
        let n_target_rows: usize = v1[0].parse().unwrap();
        let n_target_cols: usize = v1[1].parse().unwrap();
        
        let mut vec_target = Vec::new();
        for _ in 0..n_target_rows {
            let mut str_line = String::new();
            std::io::stdin().read_line(&mut str_line).unwrap();
            vec_target.push(str_line.trim().to_string());
        }
        
        // the pattern input
        let mut str_size_pattern = String::new();
        std::io::stdin().read_line(&mut str_size_pattern).unwrap();
        
        let v2: Vec<&str> = str_size_pattern.trim().split_whitespace().collect();
        let n_pattern_rows: usize = v2[0].parse().unwrap();
        let n_pattern_cols: usize = v2[1].parse().unwrap();
        
        let mut vec_pattern = Vec::new();
        for _ in 0..n_pattern_rows {
            let mut str_line = String::new();
            std::io::stdin().read_line(&mut str_line).unwrap();
            vec_pattern.push(str_line.trim().to_string());
        }
        
        // search for the begin
        let mut b_found = false;
        for i in 0..n_target_rows {
            if i + n_pattern_rows > n_target_rows {
                break;
            }
            
            for j in 0..(n_target_cols - n_pattern_cols + 1) {
                if vec_target[i][j..(j + n_pattern_cols)].eq(&vec_pattern[0]) {
                    for k in 1..n_pattern_rows {
                        // match each row
                        if vec_target[i + k][j..(j + n_pattern_cols)].eq(&vec_pattern[k]) {
                            if k == n_pattern_rows - 1 {
                                b_found = true;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    
                    if b_found {
                        break;
                    }
                }
            }
            
            // found one position
            if b_found {
                break;
            }
        }
        
        // display the result
        if b_found {
            println!("YES");
        } else {
            println!("NO");
        }
    }
}

