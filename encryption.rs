fn main() {
    // the input string
    let mut str_inp = String::new();
    std::io::stdin().read_line(&mut str_inp).unwrap();
    
    let vec_str: Vec<char> = str_inp.chars().collect();
    let n_len = vec_str.len();
    let n_squ_len = (n_len as f64).sqrt().ceil() as usize;
    
    // print matrix transport
    for c in 0..n_squ_len {
        for r in 0..n_squ_len {
            let i: usize = r * n_squ_len + c;
            if i < n_len {
                if c > 0 && r == 0 {
                    print!(" ");
                }
                
                print!("{}", vec_str[i]);
            }
        }
    }
}

