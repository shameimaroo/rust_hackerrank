fn main() {
    // the input p
    let mut str_p = String::new();
    std::io::stdin().read_line(&mut str_p).unwrap();
    let p: u64 = str_p.trim().parse().unwrap();

    // the input q
    let mut str_q = String::new();
    std::io::stdin().read_line(&mut str_q).unwrap();    
    let q: u64 = str_q.trim().parse().unwrap();
    
    let mut i = 0;
    for num in p..(q + 1) {
        // divide into two halves
        let s: Vec<char> = (num * num).to_string().chars().collect();
        let d = s.len() / 2;
        
        let str_l: String = s[0..d].iter().cloned().collect();
        let str_r: String = s[d..s.len()].iter().cloned().collect();

        let mut l: u64 = 0;
        if !str_l.is_empty() {
            l = str_l.parse().unwrap();
        }

        let mut r: u64 = 0;
        if !str_r.is_empty() {
            r = str_r.parse().unwrap();
        }
        
        // determine the minimum
        if l + r == num {
            if i > 0 {
                print!(" ");
            }
            
            i += 1;
            
            print!("{}", num);
        }
    }
    
    if i == 0 {
        println!("INVALID RANGE");
    }    
}

