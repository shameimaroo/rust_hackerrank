use std::io;

fn main() {
    
    // variable declaration
    let mut str_a = String::new();
    let mut str_b = String::new();

    // read variables
    io::stdin().read_line(&mut str_a).ok().expect("read error");
    io::stdin().read_line(&mut str_b).ok().expect("read error");

    // parse integers
    let a: i32 = str_a.trim().parse().ok().expect("parse error");
    let b: i32 = str_b.trim().parse().ok().expect("parse error");

    // print the sum
    println!("{}", a + b);
}

