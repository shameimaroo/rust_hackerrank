fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases = str_n_cases.trim().parse().unwrap();
    
    // handle each test case
    for _ in 0..n_cases {
        // the number of students and cancelation threshold
        let mut str_line = String::new();
        std::io::stdin().read_line(&mut str_line).unwrap();
        
        let vec_para: Vec<&str> = str_line.split(" ").collect();
        let _: u64 = vec_para[0].trim().parse().unwrap();
        let n_k: u64 = vec_para[1].trim().parse().unwrap();
        
        let mut n_arr: u64 = 0;
        
        let mut str_nums = String::new();
        std::io::stdin().read_line(&mut str_nums).unwrap();
        
        for str_num in str_nums.split_whitespace() {
            let num: i32 = str_num.trim().parse().unwrap();
            if num <= 0 {
                n_arr += 1;
            }
        }
        
        // display the result
        if n_arr < n_k {
            println!("YES");
        } else {
            println!("NO");
        }
    }
}

