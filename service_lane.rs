fn main() {
    // the length of way and the number of test cases
    let mut str_len_cases = String::new();
    std::io::stdin().read_line(&mut str_len_cases).unwrap();
    
    let vec_str: Vec<&str> = str_len_cases.split(" ").collect();    
    let _: u64 = vec_str[0].trim().parse().unwrap();
    let n_cases: u64 = vec_str[1].trim().parse().unwrap();
    
    // the way inputs
    let mut str_way = String::new();
    std::io::stdin().read_line(&mut str_way).unwrap();
        
    let vec_str_way: Vec<&str> = str_way.split_whitespace().collect();

    let mut vec_way = vec![];
    for str_num in vec_str_way {
        let t: u64 = str_num.trim().parse().unwrap();
        vec_way.push(t);
    }
    
    // handle each test case
    for _ in 0..n_cases {
        let mut str_line = String::new();
        std::io::stdin().read_line(&mut str_line).unwrap();
        
        let vec_entry_exit: Vec<&str> = str_line.split(" ").collect();
        let i_entry: usize = vec_entry_exit[0].trim().parse().unwrap();
        let i_exit: usize = vec_entry_exit[1].trim().parse().unwrap();
        
        // linearly search for the smallest value
        let mut val = vec_way[i_entry];
        for i in i_entry..(i_exit + 1) {
            if vec_way[i] < val {
                val = vec_way[i];
            }
        }
        
        // display the result
        println!("{}", val);
    }
}

