fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases: u64 = str_n_cases.trim().parse().unwrap();
    
    for _ in 0..n_cases {
        // the number of digits
        let mut str_n = String::new();
        std::io::stdin().read_line(&mut str_n).unwrap();
        
        let n: i64 = str_n.trim().parse().unwrap();
        
        // iterate to minimize the number of trailing 33333
        let mut a: i64 = 0;
        let mut b: i64 = 0;
        let mut b_succ = false;
        
        let ub = n / 5;
        while b <= ub {
            a = n - 5 * b;
            if a >= 0 && (a % 3) == 0 {
                a /= 3;
                b_succ = true;
                break;
            }
            
            b += 1;
        }
        
        // found one possible solution
        if b_succ {
            for _ in 0..a {
                print!("555");
            }
            
            for _ in 0..b {
                print!("33333");
            }
        } else {
            print!("-1");
        }
        
        println!("");
    }
}

