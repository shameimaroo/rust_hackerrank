fn main() {
    // the number of sticks
    let mut str_n_sticks = String::new();
    std::io::stdin().read_line(&mut str_n_sticks).unwrap();
    
    let mut n_sticks: usize = str_n_sticks.trim().parse().unwrap();
    
    // the input array
    let mut str_sticks = String::new();
    std::io::stdin().read_line(&mut str_sticks).unwrap();
    
    let vec_sticks: Vec<&str> = str_sticks.split_whitespace().collect();

    let mut vec_a = vec![0; 1000];
    for str_stick in vec_sticks {
        let t: usize = str_stick.trim().parse().unwrap();
        vec_a[t-1] += 1;
    }
    
    // display the result
    println!("{}", n_sticks);
    for cnt in vec_a {
        if cnt > 0 {
            n_sticks -= cnt;
            if n_sticks == 0 {
                break;
            }
            
            println!("{}", n_sticks);
        }
    }
}

