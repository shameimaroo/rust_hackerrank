fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases: u64 = str_n_cases.trim().parse().unwrap();
    
    // handle each test case
    for _ in 0..n_cases {
        // the number of cycles
        let mut str_n_cycles = String::new();
        std::io::stdin().read_line(&mut str_n_cycles).unwrap();
        
        let n_cycles: u32 = str_n_cycles.trim().parse().unwrap();
        
        // display the result
        if n_cycles % 2 == 0 {
            println!("{}", 2u32.pow(n_cycles / 2 + 1) - 1);
        } else {
            println!("{}", 2u32.pow((n_cycles + 1) / 2 + 1) - 2 );
        }
    }
}

