fn main() {
    // the map size
    let mut str_n = String::new();
    std::io::stdin().read_line(&mut str_n).unwrap();
    let n: usize = str_n.trim().parse().unwrap();
    
    // the map input
    let mut str_map = String::new();
    for _ in 0..n {
        let mut str_line = String::new();
        std::io::stdin().read_line(&mut str_line).unwrap();
        str_map.push_str(&str_line.trim());
    }
    
    let mut map: Vec<char> = str_map.chars().collect();

    // cavity locations
    let mut vec_loc = Vec::new();
    
    // locate cavity
    for i in 1..(n - 1) {
        for j in 1..(n - 1) {
            let ind = i * n + j;
            if map[ind] > map[ind - n] &&
               map[ind] > map[ind + n] &&
               map[ind] > map[ind - 1] &&
               map[ind] > map[ind + 1] {
                   vec_loc.push(ind);
               }
        }
    }
    
    // mark cavity
    for ind in vec_loc {
        map[ind] = 'X';
    }
    
    // display the result
    for i in 0..n {
        for j in 0..n {
            print!("{}", map[i * n + j]);
        }
        
        println!("");
    }
}

