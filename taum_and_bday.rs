fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases: usize = str_n_cases.trim().parse().unwrap();
    
    for _ in 0..n_cases {
        // the numbers of black and white gifts
        let mut str_ab = String::new();
        std::io::stdin().read_line(&mut str_ab).unwrap();
        
        let vec_ab: Vec<&str> = str_ab.trim().split_whitespace().collect();
        let a: usize = vec_ab[0].parse().unwrap();
        let b: usize = vec_ab[1].parse().unwrap();
        
        // the costs of purchasing and converting costs
        let mut str_xyz = String::new();
        std::io::stdin().read_line(&mut str_xyz).unwrap();
        
        let vec_xyz: Vec<&str> = str_xyz.trim().split_whitespace().collect();
        let x: usize = vec_xyz[0].parse().unwrap();
        let y: usize = vec_xyz[1].parse().unwrap();
        let z: usize = vec_xyz[2].parse().unwrap();
        
        // calculate the cost in total
        let mut sum: usize = 0;
        if y + z < x {
            sum += a * (y + z);
        } else {
            sum += a * x;
        }
        
        if x + z < y {
            sum += b * (x + z)
        } else {
            sum += b * y;
        }
        
        // display the result
        println!("{}", sum);
    }
}

