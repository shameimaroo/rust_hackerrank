fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases: u64 = str_n_cases.trim().parse().unwrap();
    
    for _ in 0..n_cases {
        // the input number N
        let mut str_n = String::new();
        std::io::stdin().read_line(&mut str_n).unwrap();
        
        let n: u64 = str_n.trim().parse().unwrap();
        
        let mut cnt: u64 = 0;

        let mut t = n;
        loop {
            if t == 0 {
                break;
            }
            
            let r = t % 10;
            if r > 0 && n % r == 0 {
                cnt += 1;
            }

            t /= 10;
        }

        // display the result
        println!("{}", cnt);
    }
}

