fn main() {
    // the input n
    let mut str_n = String::new();
    std::io::stdin().read_line(&mut str_n).unwrap();
    
    let n: usize = str_n.trim().parse().unwrap();
    
    // the result storage
    let mut vec_digits = vec![0; 500];
    vec_digits[0] = 1;

    let mut sz = 1;

    // factorial calculation
    for i in 2..(n + 1) {
        let mut c = 0;
        for j in 0..sz {
            let r = vec_digits[j] * i + c;
            vec_digits[j] = r % 10;
            c = r / 10;
        }
        
        while c != 0 {
            vec_digits[sz] = c % 10;
            c /= 10;
            sz += 1;
        }
    }
    
    // display the result
    for i in 0..sz {
        print!("{}", vec_digits[sz - 1 - i]);
    }
    
    println!("");
}

