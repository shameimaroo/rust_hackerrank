fn main() {
    // the number of lines
    let mut str_n = String::new();
    std::io::stdin().read_line(&mut str_n).ok().expect("input error: str_n");
    
    let n: u32 = str_n.trim().parse().ok().expect("parse error: n");
    
    // display the stair case
    for i in 0..n {
        for _ in 0..(n-i-1) {
            print!(" ");
        }

        for _ in 0..(i + 1) {
            print!("#");
        }
        
        println!("");
    }
}

