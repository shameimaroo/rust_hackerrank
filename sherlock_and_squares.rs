fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases = str_n_cases.trim().parse().unwrap();
    
    // handle each test case
    for _ in 0..n_cases {
        let mut str_line = String::new();
        std::io::stdin().read_line(&mut str_line).unwrap();
        
        let vec_line: Vec<&str> = str_line.split(" ").collect();
        let a: i64 = vec_line[0].trim().parse().unwrap();
        let b: i64 = vec_line[1].trim().parse().unwrap();
        let c = (b as f64).sqrt().floor() - (a as f64).sqrt().ceil() + 1f64;
        
        // display the result
        println!("{}", c as i64);
    }
}

