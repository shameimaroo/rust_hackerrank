fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases: u64 = str_n_cases.trim().parse().unwrap();
    
    // handle each test case
    for _ in 0..n_cases {
        // the number of stones
        let mut str_n = String::new();
        std::io::stdin().read_line(&mut str_n).unwrap();
        let mut n: u64 = str_n.trim().parse().unwrap();
        
        let mut str_a = String::new();
        std::io::stdin().read_line(&mut str_a).unwrap();
        let a: u64 = str_a.trim().parse().unwrap();

        let mut str_b = String::new();
        std::io::stdin().read_line(&mut str_b).unwrap();
        let b: u64 = str_b.trim().parse().unwrap();

        n -= 1;
        
        // try all linear combinations
        let mut vec_sum = Vec::new();
        for i in 0..(n + 1) {
            vec_sum.push(a * i + b * (n - i));
        }
        
        vec_sum.sort();
        vec_sum.dedup();
        
        // display the result
        let mut ind: usize = 0;
        for e in vec_sum {
            if ind > 0 {
                print!(" ");
            }
            
            print!("{}", e);
            ind += 1;
        }
        
        println!("");
    }
}

