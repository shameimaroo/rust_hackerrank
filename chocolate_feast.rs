fn main() {
    // the number of test cases
    let mut str_n_cases = String::new();
    std::io::stdin().read_line(&mut str_n_cases).unwrap();
    
    let n_cases: usize = str_n_cases.trim().parse().unwrap();
    
    // handle each test case
    for _ in 0..n_cases {
        // the input parameters
        let mut str_line = String::new();
        std::io::stdin().read_line(&mut str_line).unwrap();
        
        let vec_para: Vec<&str> = str_line.split_whitespace().collect();
        let n_money: u64 = vec_para[0].trim().parse().unwrap();
        let n_price: u64 = vec_para[1].trim().parse().unwrap();
        let n_trade: u64 = vec_para[2].trim().parse().unwrap();
        
        // first purchase
        let mut n_buy = n_money / n_price;
        let mut n_left = 0u64;
        let mut t = n_buy + n_left;
        let mut n = n_buy;
        
        // purchase iterations
        loop {
            if t < n_trade {
                break;
            }
            
            n_buy = t / n_trade;
            n += n_buy;
            n_left = t % n_trade;
            t = n_buy + n_left;
        }
        
        // display the result
        println!("{}", n);
    }
}

